#include "ft_active_bits.c"
#include <stdio.h>

int main(void)
{
	for (int i = 0; i < 33; i++)
	{
		printf("%u => %u\n", i, ft_active_bits(i));
	}
	printf("%u => %u\n", 0xFFFFFFFF, ft_active_bits(0xFFFFFFFF));
}
