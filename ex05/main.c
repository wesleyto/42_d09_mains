#include <stdio.h>
#include "ft_button.c"

int		main(void)
{
	printf("%d => 2\n", ft_button(1, 2, 3));
	printf("%d => 2\n", ft_button(1, 2, 2));
	printf("%d => 2\n", ft_button(2, 2, 2));
	printf("%d => 2\n", ft_button(3, 2, 1));
	printf("%d => 2\n", ft_button(3, 1, 2));
	printf("%d => 2\n", ft_button(2, 1, 3));
	printf("%d => 2\n", ft_button(2, 2, 3));
	printf("%d => 2\n", ft_button(1, 3, 2));
	printf("%d => 0\n", ft_button(-9, 0, 9));
	printf("%d => 1\n", ft_button(0, 1, 3));
	printf("%d => -8\n", ft_button(-9, -7, -8));
}
