#include <stdio.h>
#include "ft_rot42.c"

int		main(void)
{
	char str[] = "ABCD abcd WXYZ wxyz 1234 !@#$ ,./;";
	char exp[] = "QRST qrst MNOP mnop 1234 !@#$ ,./;";
	printf("Orig:\t%s\n", str);
	printf("Rot:\t%s\n", ft_rot42(str));
	printf("Exp:\t%s\n", ft_rot42(str));
}
