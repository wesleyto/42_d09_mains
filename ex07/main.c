#include "ft_collatz_conjecture.c"
#include <stdio.h>

int main(void)
{
	for (int i = -2; i < 40; i++)
	{
		printf("%d => %d\n", i, ft_collatz_conjecture(i));
	}
	return (0);
}