/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ultimator.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wto <marvin@42.fr>                         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/18 14:05:27 by wto               #+#    #+#             */
/*   Updated: 2017/08/18 14:06:42 by wto              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef		__FT_ULTIMATOR_H__
# define	__FT_ULTIMATOR_H__

/*
**
** With Windows VISTA, we were on the edge of the abyss.
** With Windows 8, we made a huge step forward.
**
** The Client: 'I have a computer running on Windows 8'
** The Technician: 'Yes...'
** The Client: 'And it doesn't work anymore'
** The Technician: 'Yeah, you already said...'
**
*/
#endif
