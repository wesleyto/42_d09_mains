#include <stdio.h>

int main(void)
{
	int a___ = 5;
	int *a__ = &a___;
	int **a_ = &a__;
	int ***a = &a_;

	int b_ = 4;
	int *b = &b_;

	int c_______ = 3;
	int *c______ = &c_______;
	int **c_____ = &c______;
	int ***c____ = &c_____;
	int ****c___ = &c____;
	int *****c__ = &c___;
	int ******c_ = &c__;
	int *******c = &c_;

	int d____ = 2;
	int *d___ = &d____;
	int **d__ = &d___;
	int ***d_ = &d__;
	int ****d = &d_;

	printf("In:\t%d %d %d %d\n", a___, b_, c_______, d____);
	printf("Exp:\t%d %d %d %d\n", 4, 2, 5, 3);
	ft_scrambler(a, b, c, d);
	printf("Got:\t%d %d %d %d\n", a___, b_, c_______, d____);
}
