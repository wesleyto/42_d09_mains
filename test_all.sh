LAST_EX=7

for num in $(seq -f "%02g" 0 $LAST_EX)
	do
		norminette -R CheckForbiddenSourceHeader ex$num/*.c ex$num/*.h
	done


for num in $(seq -f "%02g" 0 4)
	do
		gcc -Wall -Wextra -Werror -o ex$num/main ex$num/main.c
	done

gcc -Wall -Wextra -Werror -o ex05/main ex05/main.c ex04/ft_split_whitespaces.c

for num in $(seq -f "%02g" 6 $LAST_EX)
	do
		gcc -Wall -Wextra -Werror -o ex$num/main ex$num/main.c
	done


for num in $(seq -f "%02g" 0 $LAST_EX)
	do
		echo ===========================
		echo ===== Testing Ex$num =====
		echo ===========================
		./ex$num/main
		echo
	done